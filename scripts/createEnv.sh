#! /bin/bash
id -u student
if [ $? ]; then
  sudo /usr/sbin/userdel student
fi
sudo /usr/sbin/useradd -s /bin/bash student 
echo "student:student" | sudo /usr/sbin/chpasswd 
sudo /usr/sbin/usermod -a -G wheel student
if [ ! -f  /etc/network/interfaces.d/eth0.cfg ]; then
  echo "/etc/network/interfaces.d/eth0.cfg not found, can't change network config"
  exit 
fi
sed -n  '/^#/! p' /etc/network/interfaces.d/eth0.cfg > patch
sed -ie 's@^@# Old lines edited with '$0' @g' /etc/network/interfaces.d/eth0.cfg 
cat patch >> /etc/network/interfaces.d/eth0.cfg 
rm -rf patch
